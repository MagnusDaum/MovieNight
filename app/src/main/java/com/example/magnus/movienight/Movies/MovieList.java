package com.example.magnus.movienight.Movies;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Magnus on 20.01.2016.
 */
public class MovieList {
    protected List<Movie> movieItems;

    public MovieList(){
        movieItems = new ArrayList<>();
    }

    public MovieList(List<Movie> MovieItems){
        this.SetMovies(MovieItems);
    }

    public int GetLength(){
        return movieItems.size();
    }

    public Movie GetMovie(int Index){
        return movieItems.get(Index);
    }

    public List<Movie> GetMovies(){
        return movieItems;
    }

    public void SetMovies(List<Movie> MovieItems){
        this.movieItems = MovieItems;
    }

    public void Clear(){
        movieItems.clear();
    }

    public boolean isEmpty(){
        return movieItems.isEmpty();
    }

    public void Add(Movie MovieItem){
        this.movieItems.add(MovieItem);
    }

    public void Remove(int Index){
        this.movieItems.remove(Index);
    }

    public int Contains(Movie MovieItem){
        //Compares simply Title and Year
        Movie m;
        for( int i=0; i<this.GetLength(); i++ ){
            m = this.GetMovie(i);
            if ( ( m.getTitle().equals(MovieItem.getTitle()) ) && (m.getYear() == MovieItem.getYear()) ){
                return i;
            }
        }
        return -1;
    }

    public byte[] GetAsBytes(){
        ByteArrayOutputStream Result = new ByteArrayOutputStream();
        byte[] lenbyte;
        for ( int i=0; i < this.GetLength(); i++ ) {
            byte[] b = this.GetMovie(i).getAsBytes();
            lenbyte = ByteCast.int2ByteArray(b.length);
            Result.write(lenbyte,0,4);
            Result.write(b,0,b.length);
        }
        return Result.toByteArray();
    }

    public void SetFromBytes(byte[] bytedata){
        this.Clear();
        ByteArrayInputStream data = new ByteArrayInputStream(bytedata);
        byte[] lenbuf = new byte[4];
        try{
            while( ( data.read(lenbuf) ) > -1 ) {
                byte[] buf = new byte[ByteCast.ByteArray2int(lenbuf)];
                data.read(buf);
                this.Add(new Movie());
                this.GetMovie(this.GetLength()-1).setFromBytes(buf);
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }

}
