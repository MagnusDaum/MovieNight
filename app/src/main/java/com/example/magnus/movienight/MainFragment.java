package com.example.magnus.movienight;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.magnus.movienight.Movies.MovieList;

import java.util.Random;

/**
 * Created by Magnus on 29.01.2016.
 */
public class MainFragment extends Fragment {

    private MainActivity refActivity;
    private MovieListFragment movieListFragment;
    private EditText NoMoviesEditText;


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        refActivity = ((MainActivity) getActivity());

        movieListFragment = (MovieListFragment) getChildFragmentManager().findFragmentById(R.id.MovieList_Fragment);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.main_fragment, container, false);
        ((MainActivity) getActivity()).setupUI(v);

        NoMoviesEditText = (EditText) v.findViewById(R.id.NoMoviesEdit);

        v.findViewById(R.id.PickMoviesBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PickMoviesBtnClick();
            }
        });

        v.findViewById(R.id.ClearMoviesBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClearMoviesBtnClick();
            }
        });

        return v;
    }

    public void PickMoviesBtnClick() {
        int minyear = Integer.parseInt(refActivity.getPreferences().getString(getResources().getString(R.string.settings_minyear_key), String.valueOf(R.integer.settings_minyear_defvalue)));
        int maxyear = Integer.parseInt(refActivity.getPreferences().getString(getResources().getString(R.string.settings_maxyear_key), String.valueOf(R.integer.settings_maxyear_defvalue)));

        int NoMoviesToSelect;
        try {
            NoMoviesToSelect = Integer.parseInt(NoMoviesEditText.getText().toString());
        }catch (NumberFormatException e) {
            NoMoviesToSelect = R.integer.NoMoviesDefValue;
            NoMoviesEditText.setText(String.valueOf(NoMoviesToSelect), TextView.BufferType.EDITABLE);
        }

        Random rand = new Random();

        MovieList MyMovieList = refActivity.getMovieList();
        int MovieCount = MyMovieList.GetLength();
        int i = 0;
        int breakcond = 0;
        while ( (i < Math.min(NoMoviesToSelect, MovieCount)) && (breakcond < MovieCount) ) {
            int r = rand.nextInt(MovieCount-1) +1;
            if ((MyMovieList.GetMovie(r).getYear() >= minyear) && (MyMovieList.GetMovie(r).getYear() <= maxyear || maxyear<=0)) {
                if (movieListFragment.GetMovieList().Contains(MyMovieList.GetMovie(r)) < 0) {
                    movieListFragment.AddToMovieList(MyMovieList.GetMovie(r));
                    i++;
                }
            }
            breakcond++;
        }
    }

    public void ClearMoviesBtnClick() {
        movieListFragment.ClearMovieList();
    }

}
