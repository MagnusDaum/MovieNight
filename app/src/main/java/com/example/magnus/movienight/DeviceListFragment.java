package com.example.magnus.movienight;

import android.content.Context;
import android.net.wifi.p2p.WifiP2pDevice;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Magnus on 28.01.2016.
 */
public class DeviceListFragment extends ListFragment {

    private List<WifiP2pDevice> deviceList = new ArrayList<>();

    private DeviceListAdapter deviceList_Adapter = new DeviceListAdapter(deviceList);

    public List<WifiP2pDevice> GetDeviceList(){
        return deviceList;
    }

    public void SetDeviceList(List<WifiP2pDevice> dl){
        this.deviceList.clear();
        for( WifiP2pDevice d: dl){
            this.deviceList.add(d);
        }
        deviceList_Adapter.notifyDataSetChanged();
    }

    public void AddToDeviceList(WifiP2pDevice d){
        deviceList.add(d);
        deviceList_Adapter.notifyDataSetChanged();
    }

    public void RemoveDevice(int index){
        deviceList.remove(index);
        deviceList_Adapter.notifyDataSetChanged();
    }

    public void ClearDeviceList(){
        deviceList.clear();
        deviceList_Adapter.notifyDataSetChanged();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        this.setListAdapter(deviceList_Adapter);

        this.getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Interface f = FragmentUtils.getParentOfFragment(DeviceListFragment.this, Interface.class);
                if (f != null) {
                    f.OnDeviceSelected(DeviceListFragment.this.deviceList.get(position));
                }
                return true;
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.devicelist_fragment, container, false);
    }

    private class DeviceListAdapter extends BaseAdapter {

        private LayoutInflater inflater;
        private List<WifiP2pDevice> internal_deviceList;

        public DeviceListAdapter(List<WifiP2pDevice> dl) {
            this.internal_deviceList = dl;
        }

        @Override
        public int getCount() {
            return internal_deviceList.size();
        }

        @Override
        public Object getItem(int location) {
            return internal_deviceList.get(location);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (inflater == null)
                inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.devicelist_row, parent, false);
            }

            WifiP2pDevice d = (WifiP2pDevice) this.getItem(position);

            TextView name = (TextView) convertView.findViewById(R.id.devicename_TextView);
            TextView address = (TextView) convertView.findViewById(R.id.deviceaddress_TextView);

            name.setText(d.deviceName);
            address.setText(d.deviceAddress);

            return convertView;
        }

    }

    public interface Interface{
        void OnDeviceSelected(WifiP2pDevice d);
    }
}
