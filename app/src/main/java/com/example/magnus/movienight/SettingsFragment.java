package com.example.magnus.movienight;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.preference.EditTextPreference;
import android.support.v7.preference.PreferenceFragmentCompat;

/**
 * Created by Magnus on 29.01.2016.
 */
public class SettingsFragment
        extends     PreferenceFragmentCompat
        implements  SharedPreferences.OnSharedPreferenceChangeListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String s) {}

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen()
                .getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen()
                .getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        EditTextPreference pref = (EditTextPreference) findPreference(key);
        int val;
        try{
            val = Integer.valueOf(pref.getText());
        }catch (NumberFormatException e){
            if ( key.equals(getResources().getString(R.string.settings_minyear_key)) ) {
                val = getResources().getInteger(R.integer.settings_minyear_defvalue);
            }else if ( key.equals(getResources().getString(R.string.settings_maxyear_key)) ){
                val = getResources().getInteger(R.integer.settings_maxyear_defvalue);
            }else if( key.equals(getResources().getString(R.string.settings_votes_key)) ){
                val = getResources().getInteger(R.integer.settings_votes_defvalue);
            }else if( key.equals(getResources().getString(R.string.settings_extravotes_key)) ){
                val = getResources().getInteger(R.integer.settings_extravotes_defvalue);
            }else{
                val = 0;
            }
        }
        pref.setText(String.valueOf(val));

        ((MainActivity) getActivity()).OnPreferencesChanged(key);
    }
}
