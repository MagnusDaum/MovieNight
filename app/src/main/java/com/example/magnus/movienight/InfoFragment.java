package com.example.magnus.movienight;

import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Magnus on 29.01.2016.
 */
public class InfoFragment extends Fragment implements DeviceListFragment.Interface {
    private static final String DEBUG_TAG = "InfoFragment";

    private MainActivity refActivity;

    private DeviceListFragment deviceListFragment;
    private TextView WifiP2pStateTV;
    private TextView DeviceNameTV;
    private TextView DeviceAddressTV;

    private CheckBox showHostsCB;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        refActivity = (MainActivity) getActivity();

        deviceListFragment = (DeviceListFragment) getChildFragmentManager().findFragmentById(R.id.DeviceList_Fragment);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.info_fragment, container, false);
        ((MainActivity) getActivity()).setupUI(v);

        WifiP2pStateTV = (TextView) v.findViewById(R.id.wifistate_TextView);
        DeviceNameTV = (TextView) v.findViewById(R.id.devicename_TextView);
        DeviceAddressTV = (TextView) v.findViewById(R.id.deviceaddress_TextView);

        v.findViewById(R.id.discover_Button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DiscoverBtnClick();
            }
        });

        v.findViewById(R.id.TestBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( refActivity.getIsWifiP2pConnected() && !refActivity.getIsHost()) {
                    Log.d(DEBUG_TAG, "send ResendMovielistRequest");
                    refActivity.SendDataViaP2p(new byte[] { DataTransferService.COMMANDBYTE_RESENDMOVIELIST }, null); //Send To Server
                }
            }
        });


        showHostsCB = (CheckBox) v.findViewById(R.id.showHostsCB);

        return v;
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    public String GetWifiInfoAsString(boolean isWifiEnabled, boolean isWifiConnected){
        String WifiState;

        if (isWifiEnabled) {
            WifiState = getResources().getString(R.string.on);

            WifiState = WifiState + " & ";

            if (isWifiConnected){
                WifiState = WifiState + getResources().getString(R.string.connected);
            }else{
                WifiState = WifiState + getResources().getString(R.string.disconnected);
            }
        }else {
            WifiState = getResources().getString(R.string.off);
        }

        return WifiState;
    }

    public void UpdateInfoText(){
        if ( refActivity.getIsWifiP2pServiceAvailable() ) {
            String WifiState = GetWifiInfoAsString(refActivity.getIsWifiEnabled(),
                    refActivity.getIsWifiP2pConnected());
            WifiP2pStateTV.setText(WifiState);

            WifiP2pDevice d = refActivity.getWifiP2pDeviceInfo();
            DeviceNameTV.setText(d.deviceName);
            DeviceAddressTV.setText(d.deviceAddress);

            if ( showHostsCB.isChecked() ){
                deviceListFragment.SetDeviceList(refActivity.getHostingPeerList());
            }else {
                deviceListFragment.SetDeviceList(refActivity.getPeerList());
            }
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {
            UpdateInfoText();
        }
    }

    @Override
    public void OnDeviceSelected(WifiP2pDevice d) {
        Log.d(DEBUG_TAG,"selected device for connection");
        refActivity.ConnectToDevice(d);
    }

    public void DiscoverBtnClick(){
        Log.d(DEBUG_TAG,"discover click");
        refActivity.StartPeerDiscovery();
    }

}
