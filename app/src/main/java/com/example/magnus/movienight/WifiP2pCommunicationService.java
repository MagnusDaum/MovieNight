package com.example.magnus.movienight;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceInfo;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceRequest;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Magnus on 13.02.2016.
 * Background service for handling all Wifi Direct related stuff
 */
public class WifiP2pCommunicationService
        extends     Service
        implements  WifiP2pManager.ConnectionInfoListener,
                    WifiP2pManager.PeerListListener{

    private static final String DEBUG_TAG = "WifiP2pComService";

    private static final String WifiP2pDnsSdServiceInstanceName = "_whatwhat";
    private static final String WifiP2pDnsSdServiceServiceType = "_presence._tcp";

    private static final String WifiP2pDnsSdServiceRecord_isHost = "isHost";

    public static final String ACTION_SUCCESSFUL_CONNECTION = "com.example.magnus.movienight.ACTION_SUCCESSFUL_CONNECTION";
    public static final String ACTION_PEERS_CHANGED = "com.example.magnus.movienight.ACTION_PEERS_CHANGED";

    private final IBinder mBinder = new LocalBinder();
    private boolean isPeerDiscoveryActive = false;
    private boolean isServiceDiscoveryActive = false;
    private static final int StandardDiscoveryDelay = 5000;
    private static final int ConnectedDiscoveryDelay = 20000;
    private static final int InitialDiscoveryDelay = 2000;
    private static int DiscoveryDelay = StandardDiscoveryDelay;

    private BroadcastReceiver wifiP2pBCReceiver = null;
    private final IntentFilter intentFilterWifiP2p = new IntentFilter();

    private boolean isWifiEnabled = false;
    private boolean isWifiP2pConnected = false;
    private boolean isWifiP2pSupported = true;
    private WifiP2pManager mWifiP2pManager;
    private WifiP2pManager.Channel mWifiP2pChannel;
    private WifiP2pInfo mWifiP2pInfo;
    private WifiP2pDevice mDevice = new WifiP2pDevice();
    private List<WifiP2pDevice> mPeers = new ArrayList<>();
    private List<WifiP2pDevice> mHostingPeers = new ArrayList<>();
    private boolean isHost = false;

    public WifiP2pCommunicationService(){
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mWifiP2pManager = (WifiP2pManager) getSystemService(WIFI_P2P_SERVICE);
        mWifiP2pChannel = mWifiP2pManager.initialize(this, getMainLooper(), new WifiP2pManager.ChannelListener() {
            @Override
            public void onChannelDisconnected() {
                Log.d(DEBUG_TAG, "onChannelDisconnected");
            }
        });

        intentFilterWifiP2p.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        intentFilterWifiP2p.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        intentFilterWifiP2p.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        intentFilterWifiP2p.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        wifiP2pBCReceiver = new WifiP2pBroadcastReceiver();
        registerReceiver(wifiP2pBCReceiver, intentFilterWifiP2p);

        StartPeerDiscovery();

        Log.d(DEBUG_TAG, "prepareServiceDiscoveryRequest");
        prepareServiceDiscoveryRequest();
    }

    @Override
    public void onDestroy(){
        this.StopPeerDiscovery();

        unregisterReceiver(wifiP2pBCReceiver);

        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public boolean getIsWifiP2pSupported(){
        return isWifiP2pSupported;
    }

    public boolean getIsWifiEnabled() {
        return isWifiEnabled;
    }

    private void setIsWifiEnabled(boolean isWifiEnabled) {
        this.isWifiEnabled = isWifiEnabled;
    }

    public boolean getIsWifiP2pConnected() {
        return isWifiP2pConnected;
    }

    private void setIsWifiP2pConnected(boolean isWifiP2pConnected) {
        this.isWifiP2pConnected = isWifiP2pConnected;
    }
    public boolean getIsHost(){
        return isHost;
    }

    public void setIsHost(boolean isHost){
        this.isHost = isHost;
        if (isHost) {
            StopServiceDiscovery();
            registerAsLocalService();
        }else{
            mWifiP2pManager.clearLocalServices(mWifiP2pChannel,null);
            StartServiceDiscovery();
        }
    }

    public WifiP2pDevice getWifiP2pDeviceInfo() {
        return mDevice;
    }

    private void setWifiP2pDeviceInfo(WifiP2pDevice device) {
        mDevice = device;
    }

    public WifiP2pInfo getConnectionInfo() {
        return mWifiP2pInfo;
    }

    public List<WifiP2pDevice> getPeerList() {
        return mPeers;
    }

    public List<WifiP2pDevice> getHostingPeerList() {
        return mHostingPeers;
    }

    public void StartPeerDiscovery() {
        if ( ! isPeerDiscoveryActive ){
            isPeerDiscoveryActive = true;
            Thread PeerDiscoveryThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (isPeerDiscoveryActive) {
                        try {
                            Thread.sleep(InitialDiscoveryDelay);
                        }catch (InterruptedException e){
                            e.printStackTrace();
                        }
                        mWifiP2pManager.discoverPeers(mWifiP2pChannel, new WifiP2pManager.ActionListener() {
                            @Override
                            public void onSuccess() {
                                isWifiP2pSupported = true;
                            }

                            @Override
                            public void onFailure(int reasonCode) {
                                switch(reasonCode){
                                    case WifiP2pManager.BUSY:
                                        DiscoveryDelay = ConnectedDiscoveryDelay;
                                        break;
                                    default:
                                        isWifiP2pSupported = false;
                                        isPeerDiscoveryActive = false;
                                }
                                Log.d(DEBUG_TAG, "peerdiscoverythread failure: " + String.valueOf(reasonCode));
                            }
                        });
                        try {
                            Thread.sleep(DiscoveryDelay - InitialDiscoveryDelay);
                        }catch (InterruptedException e){
                            e.printStackTrace();
                        }
                    }
                }
            });
            PeerDiscoveryThread.start();
        }
    }

    public void StopPeerDiscovery() {
        isPeerDiscoveryActive = false;
        mWifiP2pManager.stopPeerDiscovery(mWifiP2pChannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
            }

            @Override
            public void onFailure(int reasonCode) {
                Log.d(DEBUG_TAG, " stopPeerDiscovery failure: " + Integer.toString(reasonCode));
            }
        });
    }

    @Override
    public void onPeersAvailable(WifiP2pDeviceList peerList) {
        Log.d(DEBUG_TAG, "onPeersAvailable peers: " + Integer.toString(peerList.getDeviceList().size()));
        for( WifiP2pDevice d: peerList.getDeviceList() ){
            if ( !this.mPeers.contains(d) ){
                this.mPeers.add(d);
            }
        }
    }

    public void DisconnectWifiP2p(){
        mWifiP2pManager.removeGroup(mWifiP2pChannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Log.d(DEBUG_TAG, "removeGroup onSuccess");
            }

            @Override
            public void onFailure(int reason) {
                Log.d(DEBUG_TAG, "removeGroup failure: " + reason);
            }
        });
    }

    public void registerAsLocalService() {
        //Called only when being server
        mWifiP2pManager.clearLocalServices(mWifiP2pChannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Map<String, String> record = new HashMap<>();
                record.put("test", "what");
                record.put(WifiP2pDnsSdServiceRecord_isHost, String.valueOf(getIsHost()));

                WifiP2pDnsSdServiceInfo serviceInfo = WifiP2pDnsSdServiceInfo.newInstance(WifiP2pDnsSdServiceInstanceName, WifiP2pDnsSdServiceServiceType, record);

                mWifiP2pManager.addLocalService(mWifiP2pChannel, serviceInfo, new WifiP2pManager.ActionListener() {
                    @Override
                    public void onSuccess() {
                        Log.d(DEBUG_TAG, "addLocalService success");
                    }

                    @Override
                    public void onFailure(int reason) {
                        Log.d(DEBUG_TAG, "addLocalService failure: " + String.valueOf(reason));
                    }
                });
            }

            @Override
            public void onFailure(int reason) {
                Log.d(DEBUG_TAG, "clearLocalServices failure: " + String.valueOf(reason));
            }
        });
    }

    private void prepareServiceDiscoveryRequest(){
        //Set Listeners in case service is discovered

        mHostingPeers.clear(); // does this make sense here??????????????????

        //For Bonjour
        WifiP2pManager.DnsSdTxtRecordListener txtListener = new WifiP2pManager.DnsSdTxtRecordListener() {
            // fullDomain: e.g "printer._ipp._tcp.local."
            @Override
            public void onDnsSdTxtRecordAvailable( String fullDomain, Map record, WifiP2pDevice device) {
                Log.d(DEBUG_TAG, "onDnsSdTxtRecordAvailable device: "+device.deviceName+" | "+device.deviceAddress);
                Log.d(DEBUG_TAG, "onDnsSdTxtRecordAvailable record: " + record.toString());
                Log.d(DEBUG_TAG, "onDnsSdTxtRecordAvailable fullDomain: " + fullDomain);
                boolean servIsHost = Boolean.parseBoolean((String) record.get(WifiP2pDnsSdServiceRecord_isHost) );
                if ( servIsHost && !mHostingPeers.contains(device) ){
                    Log.d(DEBUG_TAG, "onDnsSdTxtRecordAvailable added device!");
                    mHostingPeers.add(device);
                }else if (!servIsHost && mHostingPeers.contains(device)){
                    mHostingPeers.remove(device);
                    Log.d(DEBUG_TAG, "onDnsSdTxtRecordAvailable removed device!");
                }
            }
        };

        WifiP2pManager.DnsSdServiceResponseListener servListener = new WifiP2pManager.DnsSdServiceResponseListener() {
            @Override
            public void onDnsSdServiceAvailable(String instanceName, String registrationType, WifiP2pDevice resourceType) {
                Log.d(DEBUG_TAG, "onBonjourServiceAvailable instanceName: " + instanceName);
                Log.d(DEBUG_TAG, "onBonjourServiceAvailable registrationType: " + registrationType);
                Log.d(DEBUG_TAG, "onBonjourServiceAvailable device: "+resourceType.deviceName+" | "+resourceType.deviceAddress);
            }
        };

        Log.d(DEBUG_TAG, "setDnsSdResponseListeners");
        mWifiP2pManager.setDnsSdResponseListeners(mWifiP2pChannel, servListener, txtListener);

        /*
        //For Upnp
        WifiP2pManager.UpnpServiceResponseListener upnpListener = new WifiP2pManager.UpnpServiceResponseListener() {
            @Override
            public void onUpnpServiceAvailable(List<String> uniqueServiceNames, WifiP2pDevice srcDevice) {
                Log.d(DEBUG_TAG,"onUpnpServiceAvailable device: "+srcDevice.deviceName+" | "+srcDevice.deviceAddress);
                Log.d(DEBUG_TAG, "onUpnpServiceAvailable uniqueServiceNames: " + uniqueServiceNames.toString());
            }
        };

        Log.d(DEBUG_TAG, "setUpnpServiceResponseListeners");
        mWifiP2pManager.setUpnpServiceResponseListener(mWifiP2pChannel,upnpListener);
        */
    }

    public void StartServiceDiscovery() {
        if ( ! isServiceDiscoveryActive ){
            isServiceDiscoveryActive = true;
            Thread ServiceDiscoveryThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (isServiceDiscoveryActive) {
                        try {
                            Thread.sleep(InitialDiscoveryDelay);
                        }catch (InterruptedException e){
                            e.printStackTrace();
                        }
                        //Clearing all service requests
                        //Stops previous Service Discovery
                        mWifiP2pManager.clearServiceRequests(mWifiP2pChannel, new WifiP2pManager.ActionListener() {
                            @Override
                            public void onSuccess() {
                                //WifiP2pDnsSdServiceRequest serviceRequest = WifiP2pDnsSdServiceRequest.newInstance(); //Search for all available Bonjour Services
                                WifiP2pDnsSdServiceRequest serviceRequest = WifiP2pDnsSdServiceRequest.newInstance(WifiP2pDnsSdServiceInstanceName, WifiP2pDnsSdServiceServiceType);
                                mWifiP2pManager.addServiceRequest(mWifiP2pChannel, serviceRequest, new WifiP2pManager.ActionListener() {
                                    @Override
                                    public void onSuccess() {
                                        mWifiP2pManager.discoverServices(mWifiP2pChannel, new WifiP2pManager.ActionListener() {
                                            @Override
                                            public void onSuccess() {}

                                            @Override
                                            public void onFailure(int reason) {
                                                switch(reason){
                                                    case WifiP2pManager.BUSY:
                                                        DiscoveryDelay = ConnectedDiscoveryDelay;
                                                        break;
                                                    default:
                                                        isServiceDiscoveryActive = false;
                                                }
                                                Log.d(DEBUG_TAG, "discoverServices failure: " + String.valueOf(reason));
                                            }
                                        });
                                    }

                                    @Override
                                    public void onFailure(int reason) {
                                        Log.d(DEBUG_TAG, "addServiceRequest failure: " + String.valueOf(reason));
                                    }
                                });
                            }

                            @Override
                            public void onFailure(int reason) {
                                Log.d(DEBUG_TAG, "clearServiceRequest failure: " + String.valueOf(reason));
                            }
                        });
                        try {
                            Thread.sleep(DiscoveryDelay - InitialDiscoveryDelay);
                        }catch (InterruptedException e){
                            e.printStackTrace();
                        }
                    }
                }
            });
            ServiceDiscoveryThread.start();
        }
    }

    public void StopServiceDiscovery(){
        isServiceDiscoveryActive = false;
        mWifiP2pManager.clearServiceRequests(mWifiP2pChannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
            }

            @Override
            public void onFailure(int reason) {
                Log.d(DEBUG_TAG, "clearServiceRequest failure: " + String.valueOf(reason));
            }
        });
    }

    public void ConnectToDevice(WifiP2pDevice d){
        if ( getIsWifiP2pConnected() ){
            DisconnectWifiP2p();
        }

        WifiP2pConfig config = new WifiP2pConfig();
        config.wps.setup = WpsInfo.PBC;
        config.groupOwnerIntent = isHost ? 15 : 0; // 15 = host | 0 = not host
        config.deviceAddress = d.deviceAddress;
        Log.d(DEBUG_TAG, "ConnectToDevice to: " + config.deviceAddress + " with groupOwnerIntent: " + String.valueOf(config.groupOwnerIntent));
        mWifiP2pManager.connect(mWifiP2pChannel, config, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
            }

            @Override
            public void onFailure(int reason) {
                Log.d(DEBUG_TAG, "connect failure: " + String.valueOf(reason));
            }
        });
    }

    @Override
    public void onConnectionInfoAvailable(final WifiP2pInfo info) {
        Log.d(DEBUG_TAG, "onConnectionInfoAvailable");

        this.mWifiP2pInfo = info;

        if ( info == null ){
            return;
        }

        Log.d(DEBUG_TAG, "isGroupOwner: " + String.valueOf(info.isGroupOwner));
        Log.d(DEBUG_TAG, "groupOwnerAddress: " + String.valueOf(info.groupOwnerAddress));

        if (info.groupFormed) {
            Log.d(DEBUG_TAG, "groupFormed");
            if (info.isGroupOwner != isHost) {
                Log.d(DEBUG_TAG, "isGroupOwner != isHost! Disconnecting...");
                mWifiP2pManager.removeGroup(mWifiP2pChannel, new WifiP2pManager.ActionListener() {
                    @Override
                    public void onSuccess() {
                        Log.d(DEBUG_TAG, "removeGroup onSuccess");
                    }

                    @Override
                    public void onFailure(int reason) {
                        Log.d(DEBUG_TAG, "removeGroup failure: " + reason);
                    }
                });
            }else{
                Intent messageIntent = new Intent(WifiP2pCommunicationService.ACTION_SUCCESSFUL_CONNECTION);
                messageIntent.setAction(WifiP2pCommunicationService.ACTION_SUCCESSFUL_CONNECTION);
                sendBroadcast(messageIntent);
            }
        }
    }

    public class LocalBinder extends Binder {
        WifiP2pCommunicationService getService() {
            // Return this instance of LocalService so clients can call public methods
            return WifiP2pCommunicationService.this;
        }
    }

    private class WifiP2pBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(DEBUG_TAG, "onReceive: " + action);
            if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
                //  Indicates a change in the Wi-Fi P2P status.
                int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
                WifiP2pCommunicationService.this.setIsWifiEnabled((state == WifiP2pManager.WIFI_P2P_STATE_ENABLED));

                if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
                    Log.d(DEBUG_TAG, "wifi enabled");
                } else {
                    Log.d(DEBUG_TAG, "wifi disabled");
                }
            } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {
                // Indicates the state of Wi-Fi P2P connectivity has changed.
                NetworkInfo networkInfo = intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);
                WifiP2pCommunicationService.this.setIsWifiP2pConnected(networkInfo.isConnected());
                DiscoveryDelay = networkInfo.isConnected() ? ConnectedDiscoveryDelay : StandardDiscoveryDelay;
                if (networkInfo.isConnected()) {
                    Log.d(DEBUG_TAG, "connected");
                    if( WifiP2pCommunicationService.this.mWifiP2pManager != null){
                        WifiP2pCommunicationService.this.mWifiP2pManager.requestConnectionInfo(WifiP2pCommunicationService.this.mWifiP2pChannel, WifiP2pCommunicationService.this);
                    }
                } else {
                    Log.d(DEBUG_TAG, "disconnected");
                }
            } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {
                // Indicates a change in the list of available mPeers.
                if (WifiP2pCommunicationService.this.mWifiP2pManager != null ) {
                    WifiP2pCommunicationService.this.mWifiP2pManager.requestPeers(WifiP2pCommunicationService.this.mWifiP2pChannel, WifiP2pCommunicationService.this);
                }
            } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
                // Indicates this device's details have changed.
                WifiP2pDevice dev = intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_DEVICE);
                Log.d(DEBUG_TAG, "DeviceAddress: " + dev.deviceAddress);
                Log.d(DEBUG_TAG, "DeviceName: " + dev.deviceName);
                WifiP2pCommunicationService.this.setWifiP2pDeviceInfo(dev);
            }
        }
    }
}
