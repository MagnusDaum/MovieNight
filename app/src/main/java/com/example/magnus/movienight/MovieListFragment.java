package com.example.magnus.movienight;

import android.support.v4.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.magnus.movienight.Movies.Movie;
import com.example.magnus.movienight.Movies.MovieList;

/**
 * Created by Magnus on 28.01.2016.
 */
public class MovieListFragment extends ListFragment {

    private static String SAVE_MOVIELIST = "SAVE_MOVIELIST";

    private MovieList movieList = new MovieList();

    private MovieListAdapter movieList_Adapter = new MovieListAdapter(movieList);
    private SwipeDetector swipeDetector = new SwipeDetector();

    public enum SwipeAction {
        LR, // Left to Right
        RL, // Right to Left
        TB, // Top to bottom
        BT, // Bottom to Top
        None // when no action was detected
    }

    public MovieList GetMovieList(){
        return movieList;
    }

    public void AddToMovieList(Movie m){
        movieList.Add(m);
        movieList_Adapter.notifyDataSetChanged();
    }

    public void RemoveMovie(int index){
        movieList.Remove(index);
        movieList_Adapter.notifyDataSetChanged();
    }

    public void ClearMovieList(){
        movieList.Clear();
        movieList_Adapter.notifyDataSetChanged();
    }

    public void SetMovieListFromBytes(byte[] buf){
        movieList.SetFromBytes(buf);
        movieList_Adapter.notifyDataSetChanged();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if ( savedInstanceState != null){
            movieList.SetFromBytes(savedInstanceState.getByteArray(SAVE_MOVIELIST));
        }

        this.setListAdapter(movieList_Adapter);

        this.getListView().setOnTouchListener(swipeDetector);

        this.getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                MovieListFragment.Interface f = FragmentUtils.getParentOfFragment(MovieListFragment.this, MovieListFragment.Interface.class);
                if (f != null) {
                    Movie m = MovieListFragment.this.movieList.GetMovie(position);
                    if ( f.OnMovieRemoveRequest(m) ){
                        MovieListFragment.this.RemoveMovie(position);
                        f.OnMovieRemoved(m);
                    }
                }else {
                    MovieListFragment.this.RemoveMovie(position);
                }
                return true;
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putByteArray(SAVE_MOVIELIST, movieList.GetAsBytes());
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.movielist_fragment, container, false);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        if (swipeDetector.getAction() == SwipeAction.None) {
            Movie m = (Movie) movieList_Adapter.getItem(position);
            Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse(getResources().getString(R.string.SearchURL) + m.getTitle()));
            startActivity(browserIntent);
        }else if ((swipeDetector.getAction() == SwipeAction.RL) || (swipeDetector.getAction() == SwipeAction.LR)){
            this.RemoveMovie(position);
        }
    }

    private class MovieListAdapter extends BaseAdapter {

        private LayoutInflater inflater;
        private MovieList internal_movieList;

        public MovieListAdapter(MovieList ml) {
            this.internal_movieList = ml;
        }

        @Override
        public int getCount() {
            return internal_movieList.GetLength();
        }

        @Override
        public Object getItem(int location) {
            return internal_movieList.GetMovie(location);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (inflater == null)
                inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.movielist_row, parent, false);
            }

            ImageView thumbnail = (ImageView) convertView.findViewById(R.id.thumbnail);
            TextView title = (TextView) convertView.findViewById(R.id.title);
            TextView rating = (TextView) convertView.findViewById(R.id.rating);
            TextView genre = (TextView) convertView.findViewById(R.id.genre);
            TextView year = (TextView) convertView.findViewById(R.id.releaseYear);

            // getting movie data for the row
            Movie m = (Movie) this.getItem(position);

            // thumbnail image
            if ( m.getIcon() != null )
                thumbnail.setImageBitmap(m.getIcon());

            // title
            title.setText(m.getTitle());

            // rating
            //rating.setText(getResources().getString(R.string.rating)+": " + String.valueOf(Math.round(m.getRating() / 2.0 * 100) / 100.0));
            rating.setText(String.format(getResources().getString(R.string.rating)+": %.2f",m.getRating()));

            // genre
            String genreStr = "";
            for (String str : m.getGenre()) {
                genreStr += str + ", ";
            }
            genreStr = genreStr.length() > 0 ? genreStr.substring(0,
                    genreStr.length() - 2) : genreStr;
            genre.setText(genreStr);

            // release year
            if ( m.getYear() > 0 )
                year.setText(String.valueOf(m.getYear()));

            return convertView;
        }

    }

    private class SwipeDetector implements View.OnTouchListener {


        private static final int MIN_DISTANCE = 100;
        private float downX, downY, upX, upY;
        private SwipeAction mSwipeDetected = SwipeAction.None;

        public boolean swipeDetected() {
            return mSwipeDetected != SwipeAction.None;
        }

        public SwipeAction getAction() {
            return mSwipeDetected;
        }

        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    downX = event.getX();
                    downY = event.getY();
                    mSwipeDetected = SwipeAction.None;
                    return false; // allow other events like Click to be processed
                }
                case MotionEvent.ACTION_MOVE: {
                    upX = event.getX();
                    upY = event.getY();

                    float deltaX = downX - upX;
                    float deltaY = downY - upY;

                    if (Math.abs(deltaX) > MIN_DISTANCE){
                        if (deltaX < 0) {
                            mSwipeDetected = SwipeAction.LR; //LeftRight
                            return true;
                        }
                        if (deltaX > 0) {
                            mSwipeDetected = SwipeAction.RL; //RightLeft
                            return true;
                        }
                    }else if (Math.abs(deltaY) > MIN_DISTANCE){
                        if (deltaY < 0) {
                            mSwipeDetected = SwipeAction.TB; //TopBottm
                            return false;
                        }
                        if (deltaY > 0) {
                            mSwipeDetected = SwipeAction.BT; //BottomTop
                            return false;
                        }
                    }
                    return true;
                }
            }
            return false;
        }
    }

    public interface Interface{
        boolean OnMovieRemoveRequest(Movie m);
        void OnMovieRemoved(Movie m);
    }
}
