package com.example.magnus.movienight.Movies;

import java.nio.ByteBuffer;

/**
 * Created by Magnus on 12.02.2016.
 */
public class ByteCast {

    public static int intByteArraySize = 4;
    public static int doubleByteArraySize = 8;

    public static byte [] int2ByteArray (int value)
    {
        return ByteBuffer.allocate(intByteArraySize).putInt(value).array();
    }

    public static byte [] double2ByteArray (double value)
    {
        return ByteBuffer.allocate(doubleByteArraySize).putDouble(value).array();
    }

    public static int ByteArray2int(byte[] bytes)
    {
        return ByteBuffer.wrap(bytes).getInt();
    }

    public static double ByteArray2double(byte[] bytes)
    {
        return ByteBuffer.wrap(bytes).getDouble();
    }
}
