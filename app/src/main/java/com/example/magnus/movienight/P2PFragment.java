package com.example.magnus.movienight;

import android.net.wifi.p2p.WifiP2pDevice;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.magnus.movienight.Movies.ByteCast;
import com.example.magnus.movienight.Movies.Movie;
import com.example.magnus.movienight.Movies.MovieList;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by Magnus on 29.01.2016.
 */
public class P2PFragment
        extends Fragment
        implements  MovieListFragment.Interface,
                    DeviceListFragment.Interface{

    private static String DEBUG_TAG = "P2PFragment";
    private static String saveRemaingVotesString = "saveRemaingVotesString";

    private MainActivity refActivity;
    private MovieListFragment movieListFragment;
    private DeviceListFragment deviceListFragment;
    private RelativeLayout WaitingLayout;
    private ProgressBar WaitingPB;
    private TextView WaitingTV;
    private TextView ConnectionTV;
    private TextView VotesTV;
    private Button HostBtn;

    private Map<String,Integer> ClientVotes = new Hashtable<>();
    private int NoClients = 0;
    private int MaxVotes;
    private int ExtraVotes; //If not everyone has a WifiP2p device, add votes on host
    private int RemainingVotes;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        refActivity = (MainActivity) getActivity();
        movieListFragment = (MovieListFragment) getChildFragmentManager().findFragmentById(R.id.MovieList_Fragment);
        deviceListFragment = (DeviceListFragment) getChildFragmentManager().findFragmentById(R.id.DeviceList_Fragment);

        MaxVotes = Integer.parseInt(refActivity.getPreferences().getString(getResources().getString(R.string.settings_votes_key), String.valueOf(R.integer.settings_votes_defvalue)));
        ExtraVotes = Integer.parseInt(refActivity.getPreferences().getString(getResources().getString(R.string.settings_extravotes_key), String.valueOf(R.integer.settings_extravotes_defvalue)));

        // FIX ----------------------- Settings not read correctly at first start after installation
        if ( MaxVotes > 1000 ){
            MaxVotes = 1;
        }
        if ( ExtraVotes > 1000 ){
            ExtraVotes = 0;
        }

        if (savedInstanceState == null) {
            RemainingVotes = 0;
        }else{
            RemainingVotes = savedInstanceState.getInt(saveRemaingVotesString,0);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
            savedInstanceState.putInt(saveRemaingVotesString, RemainingVotes);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.p2p_fragment,container,false);
        ((MainActivity) getActivity()).setupUI(v);

        WaitingLayout = (RelativeLayout) v.findViewById(R.id.WaitingLayout);
        WaitingTV = (TextView) v.findViewById(R.id.WaitingTextView);
        WaitingPB = (ProgressBar) v.findViewById(R.id.WaitingProgressBar);
        ConnectionTV = (TextView) v.findViewById(R.id.ConnectionTextView);
        VotesTV = (TextView) v.findViewById(R.id.VotesTextView);

        HostBtn = (Button) v.findViewById(R.id.HostButton);
        HostBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HostButtonClick();
            }
        });

        return v;
    }

    @Override
    public void onDestroyView(){
        WaitingLayout = null;
        WaitingTV = null;
        WaitingPB = null;
        HostBtn = null;
        ConnectionTV = null;
        VotesTV = null;
        super.onDestroyView();
    }

    public void UpdateInfoText(){
        boolean isHost = refActivity.getIsHost();
        boolean isWifiEnabled = refActivity.getIsWifiEnabled();
        boolean isP2pConnected = refActivity.getIsWifiP2pConnected();
        boolean isP2pSupported = refActivity.getIsWifiP2pSupported();

        VotesTV.setText(String.format("%s: %d", getResources().getString(R.string.P2PRemainingVotes), RemainingVotes));
        if (isHost) {
            HostBtn.setText(getResources().getString(R.string.P2PHosting_End));
            ConnectionTV.setText(String.format("%s: %d", getResources().getString(R.string.P2PConnectedDevices), NoClients));
        }else{
            if ( isP2pConnected ){
                HostBtn.setText(getResources().getString(R.string.disconnect));
                ConnectionTV.setText(String.format( "%s: %s", getResources().getString(R.string.P2PConnectedTo), refActivity.getConnectionInfo().groupOwnerAddress.toString() ));
            }else{
                HostBtn.setText(getResources().getString(R.string.P2PHosting_Start));
                ConnectionTV.setText(String.format( "%s: -", getResources().getString(R.string.P2PConnectedTo) ));
            }
        }

        if ( deviceListFragment.getView() != null && movieListFragment.getView() != null) {
            deviceListFragment.SetDeviceList(refActivity.getHostingPeerList());
            /*
            Visibility:
            Wifi off / P2P not supported: Show warning
            Not connected as Client and Peers available: DeviceList
            Not connected else: Progressbar
            Connected and (Clients or MovieList available): Movielist
            Connected else: Progressbar
             */
            WaitingPB.setVisibility((isWifiEnabled && isP2pSupported) ? View.VISIBLE : View.GONE); //This is overwritten by enclosing Layout
            if ( !isWifiEnabled || !isP2pSupported ){
                movieListFragment.getView().setVisibility( View.GONE );
                deviceListFragment.getView().setVisibility(View.GONE);
                WaitingLayout.setVisibility(View.VISIBLE);
                if (!isWifiEnabled){
                    WaitingTV.setText(getResources().getString(R.string.P2PWifiDisabled));
                }else {
                    WaitingTV.setText(getResources().getString(R.string.P2PNotSupported));
                }
            }else if ( ! isP2pConnected ){
                movieListFragment.getView().setVisibility( View.GONE );
                if ( !isHost && !deviceListFragment.GetDeviceList().isEmpty() ){
                    deviceListFragment.getView().setVisibility(View.VISIBLE);
                    WaitingLayout.setVisibility(View.GONE);
                }else {
                    deviceListFragment.getView().setVisibility(View.GONE);
                    WaitingLayout.setVisibility(View.VISIBLE);
                    WaitingTV.setText(isHost ? getResources().getString(R.string.P2PWaitForClients) : getResources().getString(R.string.P2PWaitForHosts));
                }
            }else{
                deviceListFragment.getView().setVisibility( View.GONE );
                if ( isHost && (NoClients > 0) || ( !isHost && !movieListFragment.GetMovieList().isEmpty() ) ){
                    movieListFragment.getView().setVisibility(View.VISIBLE);
                    WaitingLayout.setVisibility(View.GONE);
                }else{
                    movieListFragment.getView().setVisibility(View.GONE);
                    WaitingLayout.setVisibility(View.VISIBLE);
                    WaitingTV.setText(isHost ? getResources().getString(R.string.P2PWaitForClients) : getResources().getString(R.string.P2PWaitForMovielist));
                }
            }
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {
            UpdateInfoText();
        }
    }

    public void OnPreferencesChanged(String key){
        if ( refActivity.getIsHost() ) {
            if (key.equals(getResources().getString(R.string.settings_minyear_key)) || key.equals(getResources().getString(R.string.settings_maxyear_key))) {
                movieListFragment.ClearMovieList();
                FillInMovies(CalcMinMovies());
            } else if (key.equals(getResources().getString(R.string.settings_votes_key)) || key.equals(getResources().getString(R.string.settings_extravotes_key))) {
                MaxVotes = Integer.parseInt(refActivity.getPreferences().getString(getResources().getString(R.string.settings_votes_key), String.valueOf(R.integer.settings_votes_defvalue)));
                ExtraVotes = Integer.parseInt(refActivity.getPreferences().getString(getResources().getString(R.string.settings_extravotes_key), String.valueOf(R.integer.settings_extravotes_defvalue)));
                RemainingVotes = MaxVotes * (ExtraVotes + 1);
                FillInMovies(CalcMinMovies());
            }
        }
    }

    @Override
    public void OnDeviceSelected(WifiP2pDevice d) {
        Log.d(DEBUG_TAG,"selected device for connection");
        refActivity.ConnectToDevice(d);
    }

    @Override
    public boolean OnMovieRemoveRequest(Movie m) {
        Log.d(DEBUG_TAG, "onmovieremoved");
        if ( refActivity.getIsWifiP2pConnected() ) {
            if (refActivity.getIsHost()) {
                if (RemainingVotes > 0) {
                    RemainingVotes--;
                    return true;
                }
            } else {
                SendDeleteMovieRequest(m);
            }
        }
        return false;
    }

    @Override
    public void OnMovieRemoved(Movie m){
        SendMovielist("");
    }

    public void OnClientsChanged(int PeerSize){
        if ( refActivity.getIsHost() ) {
            NoClients = PeerSize;
            Log.d(DEBUG_TAG,"New Number of clients: "+String.valueOf(NoClients));
            UpdateInfoText();
            FillInMovies(CalcMinMovies());
        }
    }

    public void OnPeersChanged(){
        UpdateInfoText();
    }

    private int CalcMinMovies(){
        if ( ! refActivity.getIsHost() ) {
            return 0;
        }
        int minMovies = 1 + RemainingVotes;
        Log.d(DEBUG_TAG,"MinMovies: "+String.valueOf(minMovies));
        minMovies += MaxVotes * NoClients;
        Log.d(DEBUG_TAG, "MinMovies: " + String.valueOf(minMovies));
        for (Map.Entry<String, Integer> entry : ClientVotes.entrySet()) {
            minMovies -= entry.getValue();
        }
        Log.d(DEBUG_TAG, "MinMovies: " + String.valueOf(minMovies));

        return minMovies;
    }

    private void FillInMovies(int minMovies){
        if (movieListFragment.GetMovieList().GetLength() >= minMovies || ! refActivity.getIsHost()) {
            return;
        }

        Random rand = new Random();
        MovieList MyMovieList = refActivity.getMovieList();
        int MovieCount = MyMovieList.GetLength();

        int MinYear = Integer.parseInt(refActivity.getPreferences().getString(getResources().getString(R.string.settings_minyear_key), String.valueOf(R.integer.settings_minyear_defvalue)));
        int MaxYear = Integer.parseInt(refActivity.getPreferences().getString(getResources().getString(R.string.settings_maxyear_key), String.valueOf(R.integer.settings_maxyear_defvalue)));

        int breakcond = 0;
        while ((movieListFragment.GetMovieList().GetLength() < Math.min(minMovies, MovieCount)) && (breakcond < MovieCount)) {
            Movie m = MyMovieList.GetMovie( rand.nextInt(MovieCount - 1) + 1 );
            if ( (m.getYear() >= MinYear) && (m.getYear() <= MaxYear || MaxYear <= 0) ) {
                if (movieListFragment.GetMovieList().Contains(m) < 0) {
                    movieListFragment.AddToMovieList(m);
                }
            }
            breakcond++;
        }
        SendMovielist("");
        SendRemainingVotes("");
    }

    private void HostButtonClick(){
        ClientVotes.clear();
        movieListFragment.ClearMovieList();
        if ( refActivity.getIsHost() ) {
            refActivity.ChangeHosting();
            RemainingVotes = 0;
        }else {
            if (refActivity.getIsWifiP2pConnected()){
                refActivity.Disconnect();
                RemainingVotes = 0;
            }else {
                refActivity.ChangeHosting();
                RemainingVotes = MaxVotes * (ExtraVotes + 1);
            }
        }
        UpdateInfoText();
    }

    public void SendDeleteMovieRequest(Movie m){
        Log.d(DEBUG_TAG,"SendDeleteMovieRequest");
        if ( refActivity.getIsWifiP2pConnected() && ! refActivity.getIsHost() ) {
            ByteArrayOutputStream message = new ByteArrayOutputStream();
            message.write(DataTransferService.COMMANDBYTE_DELETEMOVIE);
            byte[] b = m.getAsBytes();
            message.write(b, 0, b.length);
            Log.d(DEBUG_TAG, "send DeleteMovieRequest | len: "+b.length);
            refActivity.SendDataViaP2p(message.toByteArray(), ""); //Send To Server
        }
    }

    private void SendMovielist(String DeviceAddress){
        if ( refActivity.getIsWifiP2pConnected() && refActivity.getIsHost() ) {
            ByteArrayOutputStream message = new ByteArrayOutputStream();
            message.write(DataTransferService.COMMANDBYTE_NEWMOVIELIST);
            byte[] b = movieListFragment.GetMovieList().GetAsBytes();
            message.write(b, 0, b.length);
            Log.d(DEBUG_TAG, "send Movielist | len: " + b.length);
            refActivity.SendDataViaP2p(message.toByteArray(), DeviceAddress);
        }
    }

    private void SendRemainingVotes(String DeviceAddress){
        /*
        As this Fragment doesn't know every Client, but only the ones who voted:
        If DeviceAddress is empty, send to everyone:
        Send MaxVotes to everyone, and afterwards send correct value to known Clients,
        which overwrites the MaxVotes value
         */
        if ( refActivity.getIsWifiP2pConnected() && refActivity.getIsHost() ) {
            Log.d(DEBUG_TAG, "send RemainingVotes to: "+DeviceAddress);
            {
                //Here, send value to DeviceAddress
                //If DeviceAddress is empty, MaxVotes is send to everyone
                ByteArrayOutputStream message = new ByteArrayOutputStream();
                message.write(DataTransferService.COMMANDBYTE_REMAININGVOTES);
                byte[] b;
                if ( ClientVotes.containsKey(DeviceAddress) ) {
                    b = ByteCast.int2ByteArray(MaxVotes - ClientVotes.get(DeviceAddress));
                } else {
                    b = ByteCast.int2ByteArray(MaxVotes);
                }
                message.write(b, 0, b.length);

                refActivity.SendDataViaP2p(message.toByteArray(), DeviceAddress);
            }
            if ( DeviceAddress.isEmpty() ){
                //Here overwrite with correct value for known clients
                for(Map.Entry<String,Integer> e: ClientVotes.entrySet()){
                    ByteArrayOutputStream message = new ByteArrayOutputStream();
                    message.write(DataTransferService.COMMANDBYTE_REMAININGVOTES);
                    byte[] b = ByteCast.int2ByteArray(MaxVotes - e.getValue());
                    message.write(b, 0, b.length);
                    refActivity.SendDataViaP2p(message.toByteArray(), e.getKey());
                }
            }
        }
    }

    public void OnDataReceived(byte[] bytedata, String DeviceAddress){
        byte CommandByte = bytedata[0];
        byte[] BufferData;
        if ( bytedata.length > 1){
            BufferData = Arrays.copyOfRange(bytedata, 1, bytedata.length );
            Log.d(DEBUG_TAG, "datareceived bytedata has len: " + String.valueOf(BufferData.length));
        }else{
            BufferData = new byte[0];
        }
        if ( refActivity.getIsHost() ) {
            if (CommandByte == DataTransferService.COMMANDBYTE_RESENDMOVIELIST) {
                Log.d(DEBUG_TAG, "datareceived: resend movies");
                SendMovielist(DeviceAddress);
                SendRemainingVotes(DeviceAddress);
            } else if (CommandByte == DataTransferService.COMMANDBYTE_DELETEMOVIE) {
                Log.d(DEBUG_TAG, "datareceived: delete movie");
                Movie m = new Movie();
                m.setFromBytes(BufferData);
                Log.d(DEBUG_TAG, "datareceived: delete movie: title: " + String.valueOf(m.getTitle()));
                int position = movieListFragment.GetMovieList().Contains(m);
                Log.d(DEBUG_TAG, "datareceived: delete movie: pos: " + String.valueOf(position));
                if ( position > -1 && ( !ClientVotes.containsKey(DeviceAddress) || ClientVotes.get(DeviceAddress) < MaxVotes ) ) {
                        Log.d(DEBUG_TAG, "deleted movie");
                        movieListFragment.RemoveMovie(position);
                        if (!ClientVotes.containsKey(DeviceAddress)) {
                            ClientVotes.put(DeviceAddress, 1);
                        } else {
                            ClientVotes.put(DeviceAddress, ClientVotes.get(DeviceAddress) + 1);
                        }
                        SendMovielist(""); //Send updated list to all
                        SendRemainingVotes(DeviceAddress);
                }
            } else if (CommandByte == DataTransferService.COMMANDBYTE_DEVICEADDRESS) {
                Log.d(DEBUG_TAG, "datareceived: deviceaddress");
                try {
                    String SentDeviceAddress = new String(BufferData, "UTF-8");
                    ClientVotes.put(SentDeviceAddress,0);
                }catch(UnsupportedEncodingException e){
                    e.printStackTrace();
                }
            }
        }else{
            if (CommandByte == DataTransferService.COMMANDBYTE_NEWMOVIELIST) {
                Log.d(DEBUG_TAG, "datareceived: new movielist");
                movieListFragment.SetMovieListFromBytes(BufferData);
                Log.d(DEBUG_TAG, "movielist len: " + String.valueOf(movieListFragment.GetMovieList().GetLength()));
                UpdateInfoText();
            }else if (CommandByte == DataTransferService.COMMANDBYTE_REMAININGVOTES){
                Log.d(DEBUG_TAG, "datareceived: remaining votes");
                RemainingVotes = ByteCast.ByteArray2int(BufferData);
                Log.d(DEBUG_TAG, "remaining votes: "+String.valueOf(RemainingVotes));
                UpdateInfoText();
            }else if (CommandByte == DataTransferService.COMMANDBYTE_ENDCONNECTION){
                Toast.makeText(getActivity(),"Server ended connection!", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
