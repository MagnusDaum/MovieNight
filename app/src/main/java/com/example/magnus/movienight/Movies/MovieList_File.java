package com.example.magnus.movienight.Movies;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.example.magnus.movienight.R;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.util.ArrayList;

/**
 * Created by Magnus on 20.01.2016.
 */
public class MovieList_File extends MovieList {

    public MovieList_File(Context context){
        XmlResourceParser xrp = context.getResources().getXml(R.xml.moviedata);
        try {
            xrp.next();
            int eventType = xrp.getEventType();
            Movie NewMovieItem = new Movie();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG){
                    switch (xrp.getName()) {
                        case "movie":
                            NewMovieItem = new Movie();
                            NewMovieItem.setGenre(new ArrayList<String>());
                            break;
                        case "imgsrc":
                            NewMovieItem.setThumbnailUrl(xrp.getAttributeValue(null, "attr"));
                            int id = context.getResources().getIdentifier(xrp.getAttributeValue(null, "attr"),"drawable",context.getPackageName());
                            Bitmap b = BitmapFactory.decodeResource(context.getResources(), id);
                            NewMovieItem.setIcon( b );
                            b = null;
                            break;
                        case "rating":
                            NewMovieItem.setRating(Double.parseDouble(xrp.getAttributeValue(null, "attr")));
                            break;
                        case "url":
                            NewMovieItem.setUrl(xrp.getAttributeValue(null, "attr"));
                            break;
                        case "title":
                            NewMovieItem.setTitle(xrp.getAttributeValue(null, "attr"));
                            break;
                        case "year":
                            NewMovieItem.setYear( Integer.parseInt( xrp.getAttributeValue(null, "attr") ) );
                            break;
                    }
                }else if (eventType == XmlPullParser.END_TAG && xrp.getName().equals("movie")){
                    this.movieItems.add(NewMovieItem);
                }
                eventType = xrp.next();
            }
        }catch (XmlPullParserException | java.io.IOException e){
            e.printStackTrace();
        }
    }
}
