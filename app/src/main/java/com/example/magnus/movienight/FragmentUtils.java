package com.example.magnus.movienight;

import android.support.v4.app.Fragment;

import java.nio.ByteBuffer;

/**
 * Created by Magnus on 26.01.2016.
 */
public class FragmentUtils {

    public static <T> T getParentOfFragment(Fragment fragment, Class<T> parentClass) {
        Fragment parentFragment = fragment.getParentFragment();
        if (parentClass.isInstance(parentFragment)) {
            //Checked by runtime methods
            //noinspection unchecked
            return (T) parentFragment;
        } else if (parentClass.isInstance(fragment.getActivity())) {
            //Checked by runtime methods
            //noinspection unchecked
            return (T) fragment.getActivity();
        }
        return null;
    }

}
