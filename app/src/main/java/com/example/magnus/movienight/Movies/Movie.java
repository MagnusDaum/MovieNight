package com.example.magnus.movienight.Movies;

/**
 * Created by Magnus on 20.01.2016.
 */

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class Movie {
    private String title = "";
    private String url = "";
    private String thumbnailUrl = "";
    private int year = 0;
    private double rating = 0;
    private ArrayList<String> genre = new ArrayList<String>();
    private Bitmap icon = null;

    public Movie() {
    }

    public Movie(String title, String url, String thumbnailUrl, int year, double rating,
                 ArrayList<String> genre, Bitmap icon) {
        this.title = title;
        this.url = url;
        this.thumbnailUrl = thumbnailUrl;
        this.year = year;
        this.rating = rating;
        this.genre = genre;
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public ArrayList<String> getGenre() {
        return genre;
    }

    public void setGenre(ArrayList<String> genre) {
        this.genre = genre;
    }

    public Bitmap getIcon() {
        return icon;
    }

    public void setIcon(Bitmap icon) {
        this.icon = icon;
    }

    public byte[] getAsBytes(){
        ByteArrayOutputStream Result = new ByteArrayOutputStream();
        byte[] lenbyte;
        int len;

        len = this.getTitle().getBytes().length;
        lenbyte = ByteCast.int2ByteArray(len);
        Result.write(lenbyte,0,4);
        Result.write(this.getTitle().getBytes(),0,len);

        len = this.getUrl().getBytes().length;
        lenbyte = ByteCast.int2ByteArray(len);
        Result.write(lenbyte,0,4);
        Result.write(this.getUrl().getBytes(),0,len);

        len = ByteCast.int2ByteArray(this.getYear()).length;
        lenbyte = ByteCast.int2ByteArray(len);
        Result.write(lenbyte,0,4);
        Result.write(ByteCast.int2ByteArray(this.getYear()),0,len);

        len = ByteCast.double2ByteArray(this.getRating()).length;
        lenbyte = ByteCast.int2ByteArray(len);
        Result.write(lenbyte, 0, 4);
        Result.write(ByteCast.double2ByteArray(this.getRating()), 0, len);

        if ( this.getIcon() != null ){
            ByteArrayOutputStream bmpstr = new ByteArrayOutputStream();
            this.getIcon().compress(Bitmap.CompressFormat.PNG, 100, bmpstr);
            len = bmpstr.toByteArray().length;
            lenbyte = ByteCast.int2ByteArray(len);
            Result.write(lenbyte, 0, 4);
            Result.write(bmpstr.toByteArray(), 0, len);
            //ByteBuffer byteBuffer = ByteBuffer.allocate(len);
            //m.getIcon().copyPixelsToBuffer(byteBuffer);
            //Result.write(byteBuffer.array(), 0, len);
        }else{
            lenbyte = ByteCast.int2ByteArray(0);
            Result.write(lenbyte, 0, 4);
        }
        return Result.toByteArray();
    }

    public void setFromBytes(byte[] bytedata){
        ByteArrayInputStream data = new ByteArrayInputStream(bytedata);
        byte[] lenbuf = new byte[4];
        try{
            data.read(lenbuf);
            byte[] buf = new byte[ByteCast.ByteArray2int(lenbuf)];
            data.read(buf);
            this.setTitle(new String(buf, "UTF-8"));

            data.read(lenbuf);
            buf = new byte[ByteCast.ByteArray2int(lenbuf)];
            data.read(buf);
            this.setUrl(new String(buf, "UTF-8"));

            data.read(lenbuf);
            buf = new byte[ByteCast.ByteArray2int(lenbuf)];
            data.read(buf);
            this.setYear(ByteCast.ByteArray2int(buf));

            data.read(lenbuf);
            buf = new byte[ByteCast.ByteArray2int(lenbuf)];
            data.read(buf);
            this.setRating(ByteCast.ByteArray2double(buf));

            data.read(lenbuf);
            buf = new byte[ByteCast.ByteArray2int(lenbuf)];
            data.read(buf);
            Bitmap b = BitmapFactory.decodeByteArray(buf, 0, ByteCast.ByteArray2int(lenbuf));
            //ByteBuffer buffer = ByteBuffer.wrap(buf);
            //Bitmap b = Bitmap.createBitmap(80,80,Bitmap.Config.ARGB_8888);
            //b.copyPixelsFromBuffer(buffer);
            this.setIcon( b );

        }catch (IOException e){
            e.printStackTrace();
        }
    }

}