package com.example.magnus.movienight.Movies;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Magnus on 20.01.2016.
 */
public class MovieList_Website extends MovieList {

    public MovieList_Website(Context context, String url) {
        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);

        //Load Website at Start
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            new DownloadWebpageTask().execute(url);
        }
    }

    //IMDB specific for now
    //Populates movieItems with entries
    private boolean ExtractMovies(String Data){
        if ( Data.isEmpty() ) {
            return false;
        }
        this.movieItems.clear();
        for (int MovieID = 1; MovieID < 250; MovieID++) {
            Movie NewMovieItem = new Movie();
            int idx, a, b;

            NewMovieItem.setGenre(new ArrayList<String>()); //-----

            idx = Data.indexOf("<span name=\"rk\" data-value=\"" + Integer.toString(MovieID) + "\">");
            a = Data.indexOf("<a href=", idx);
            a = Data.indexOf("<a href=", a + 1);
            a = Data.indexOf(">", a);
            b = Data.indexOf("<", a);
            NewMovieItem.setTitle(Data.substring(a + 1, b));

            a = Data.indexOf("<span class=\"secondaryInfo\">", idx);
            a = Data.indexOf("(", a);
            b = Data.indexOf(")", a);
            NewMovieItem.setYear(Integer.parseInt(Data.substring(a + 1, b)));

            a = Data.indexOf("<span name=\"ir\" data-value=\"", idx);
            a = Data.indexOf("data-value", a);
            a = Data.indexOf("\"", a);
            b = Data.indexOf("\"", a + 1);
            NewMovieItem.setRating(Double.parseDouble(Data.substring(a + 1, b)));

            a = Data.indexOf("<img src=", idx);
            a = Data.indexOf("\"", a);
            b = Data.indexOf("\"", a + 1);
            NewMovieItem.setThumbnailUrl(Data.substring(a + 1, b));

            a = Data.indexOf("<a href=", idx);
            a = Data.indexOf("\"", a);
            b = Data.indexOf("\"", a + 1);
            NewMovieItem.setUrl("http://www.imdb.com" + Data.substring(a + 1, b));

            this.movieItems.add(NewMovieItem);

            new DownloadImageTask(this.movieItems.size()-1).execute(NewMovieItem.getThumbnailUrl());
        }
        return true;
    }

    private boolean SetMovieImage(int Position, Bitmap bmp) {
        if ( bmp == null)
            return false;
        this.movieItems.get(Position).setIcon(bmp);
        return true;
    }

    // Uses AsyncTask to create a task away from the main UI thread. This task takes a
    // URL string and uses it to create an HttpUrlConnection.
    private class DownloadWebpageTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            // params comes from the execute() call: params[0] is the url.
            try {
                return downloadUrl(urls[0]);
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            ExtractMovies(result);
        }
    }

    // Given a URL, establishes an HttpUrlConnection and retrieves
    // the web page content as a InputStream, which it returns as
    // a string.
    private String downloadUrl(String myurl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            //int response = conn.getResponseCode();
            is = conn.getInputStream();

            // Convert the InputStream into a string
            return InputStreamToString(is);

            // Makes sure that the InputStream is closed after the app is
            // finished using it.
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    // Reads an InputStream and converts it to a String.
    private String InputStreamToString(InputStream stream) throws IOException {
        BufferedReader buffreader = new BufferedReader( new InputStreamReader(stream, "UTF-8") );
        String line;
        StringBuilder text = new StringBuilder();

        try {
            while (( line = buffreader.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
        } catch (IOException e) {
            return null;
        }
        return text.toString();
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        private int position;

        public DownloadImageTask(int position) {
            this.position = position;
        }

        protected Bitmap doInBackground(String... urls) {
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urls[0]).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            SetMovieImage(position, result);
        }
    }
}