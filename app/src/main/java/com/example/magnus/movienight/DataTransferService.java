package com.example.magnus.movienight;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.example.magnus.movienight.Movies.ByteCast;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * Created by Magnus on 23.01.2016.
 */
public class DataTransferService extends Service {
    private static final String DEBUG_TAG = "DataTransferService";

    private static final int SOCKET_TIMEOUT = 5000;
    private static final int BUFFER_SIZE = 8192;
    public static final int SOCKET_PORT = 8987;

    public static final String ACTION_RECEIVED_DATA = "com.example.magnus.movienight.ACTION_RECEIVED_DATA";
    public static final String ACTION_NUMBER_OF_CLIENTS = "com.example.magnus.movienight.ACTION_NUMBER_OF_CLIENTS";
    public static final String EXTRAS_BYTEDATA = "BYTEDATA";
    public static final String EXTRAS_NUMBER_OF_CLIENTS = "NUMBER_OF_CLIENTS";
    public static final String EXTRAS_CLIENT_ADDRESS = "CLIENT_ADDRESS";

    private static final byte[] SENDPACKAGE_HEADER = new byte[] { (byte) 0xaa, (byte) 0x55, (byte) 0xaa, (byte) 0x55};
    private static final byte[] SENDPACKAGE_FOODER = new byte[] { (byte) 0x55, (byte) 0xaa, (byte) 0x55, (byte) 0xaa};
    public static final byte COMMANDBYTE_RESENDMOVIELIST = 0x00;
    public static final byte COMMANDBYTE_NEWMOVIELIST = 0x01;
    public static final byte COMMANDBYTE_DELETEMOVIE = 0x02;
    public static final byte COMMANDBYTE_ENDCONNECTION = 0x03;
    public static final byte COMMANDBYTE_REMAININGVOTES = 0x04;
    public static final byte COMMANDBYTE_DEVICEADDRESS = 0x05;

    private final IBinder mBinder = new LocalBinder();
    private Thread ServerThread;
    private ServerSocket serverSocket;
    private boolean isThreadsActive;
    private boolean isInServerMode = false;

    private Map<Socket,String> Clients = new Hashtable<>();

    private List<OutputStream> LockedOutStreams = new ArrayList<>();

    public DataTransferService() {
    }

    @Override
    public void onCreate(){
        super.onCreate();
        this.ServerThread = new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d(DEBUG_TAG,"serverthread run");
                try {
                    serverSocket = new ServerSocket(SOCKET_PORT);
                    Log.d(DEBUG_TAG, "ServerSocket opened");
                    while (isThreadsActive) {
                        Socket NewClient = serverSocket.accept();
                        Log.d(DEBUG_TAG, "Server: new Client accepted");
                        Clients.put(NewClient, "");
                        Runnable clientHandler = new ClientHandler(NewClient,NewClient.getInetAddress());
                        new Thread(clientHandler).start();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }finally {
                    if ( serverSocket != null && !serverSocket.isClosed() ) {
                        try {
                            serverSocket.close();
                        }catch (IOException e){
                            e.printStackTrace();
                        }
                    }
                    serverSocket = null;
                    Log.d(DEBUG_TAG, "ServerSocket closed");
                }
            }
        });
    }

    @Override
    public void onDestroy(){
        StopSocketThreads();

        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    private void UpdateClientNumber(){
        Intent messageIntent = new Intent(DataTransferService.ACTION_NUMBER_OF_CLIENTS);
        messageIntent.setAction(DataTransferService.ACTION_NUMBER_OF_CLIENTS);
        messageIntent.putExtra(DataTransferService.EXTRAS_NUMBER_OF_CLIENTS, Clients.size());
        sendBroadcast(messageIntent);
        Log.d(DEBUG_TAG,"sent update Client number intent");
    }

    public void StopSocketThreads(){
        Log.d(DEBUG_TAG,"StopSocketThreads");
        byte[] b = new byte[] { COMMANDBYTE_ENDCONNECTION };
        SendViaSocket(b, "");

        isThreadsActive = false;

        for( Socket c : Clients.keySet()) {
            if ( c != null && !c.isClosed() ) {
                try {
                    c.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        Clients.clear();

        if ( serverSocket != null && !serverSocket.isClosed() ) {
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        serverSocket = null;

        try {
            ServerThread.join(); // Wait for Thread to finish
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void SetToServerMode(){
        if( !isInServerMode ) {
            isInServerMode = true;
            StopSocketThreads();
            isThreadsActive = true;
            this.ServerThread.start();
            Log.d(DEBUG_TAG, "started serverthread");
        }else{
            Log.d(DEBUG_TAG, "serverthread already running");
        }
    }

    public void SetToClientMode(InetAddress HostAddress, String DeviceAddress){
        StopSocketThreads();
        isThreadsActive = true;
        isInServerMode = false;
        final Socket s = new Socket();
        Clients.put(s, DeviceAddress);
        Runnable clientHandler = new ClientHandler(s,HostAddress);
        new Thread(clientHandler).start();

        //Send DeviceAddress to Server as soon as possible
        ByteArrayOutputStream message = new ByteArrayOutputStream();
        message.write(DataTransferService.COMMANDBYTE_DEVICEADDRESS);
        byte[] b = DeviceAddress.getBytes();
        message.write(b, 0, b.length);
        final byte[] bytedata = message.toByteArray();

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while(s.isClosed() || !s.isConnected()){
                        try {
                            Thread.sleep(1000);
                        }catch(InterruptedException e){
                            e.printStackTrace();
                        }
                    }
                    Log.d(DEBUG_TAG, "Trying to send own deviceaddress to: " + s.getInetAddress());
                    final OutputStream os = s.getOutputStream();
                    if (os != null) {
                        BlockingSocketWrite(os, bytedata);
                    } else {
                        Log.d(DEBUG_TAG, "No valid OutputStream");
                    }
                }catch(IOException e){
                    e.printStackTrace();
                }
            }
        });
        t.start();
    }

    private void OnReceivedDataViaSocket(byte[] bytedata, Socket Client){
        Log.d(DEBUG_TAG, "Data received with len: " + String.valueOf(bytedata.length));

        //Intent messageIntent = new Intent(this, MainActivity.class);
        Intent messageIntent = new Intent(DataTransferService.ACTION_RECEIVED_DATA);
        messageIntent.setAction(DataTransferService.ACTION_RECEIVED_DATA);
        messageIntent.putExtra(DataTransferService.EXTRAS_BYTEDATA, bytedata);
        messageIntent.putExtra(DataTransferService.EXTRAS_CLIENT_ADDRESS, Clients.get(Client));
        sendBroadcast(messageIntent);
    }

    public void SendViaSocket(final byte[] bytedata, String DeviceAddress){
        Log.d(DEBUG_TAG, "sendviasocket | to: " + DeviceAddress + "|| empty: "+String.valueOf(DeviceAddress.isEmpty())+ " | len: " + String.valueOf(bytedata.length));
        try {
            if ( DeviceAddress.isEmpty() ){
                //Send to everyone -> Traverse Clients and send message to every address
                Log.d(DEBUG_TAG, "sendviasocket | to: everyone | len: " + String.valueOf(Clients.entrySet().size()));
                for( Map.Entry<Socket,String> e: Clients.entrySet() ){
                    if (!e.getValue().isEmpty()){
                        SendViaSocket(bytedata, e.getValue());
                    }
                }
            }else if ( Clients.containsValue(DeviceAddress) ){
                Socket s = null;
                for( Map.Entry<Socket,String> e: Clients.entrySet() ){
                    if ( DeviceAddress.equals(e.getValue()) ){
                        s = e.getKey();
                    }
                }
                if ( s != null && !s.isClosed() && s.isConnected() ) {
                    Log.d(DEBUG_TAG,"Trying to send to: "+s.getInetAddress());
                    final OutputStream os = s.getOutputStream();
                    if( os != null ) {
                        Thread SenderThread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                BlockingSocketWrite( os, bytedata );
                            }
                        });
                        SenderThread.start();
                    }else{
                        Log.d(DEBUG_TAG, "No valid OutputStream");
                    }
                }else{
                    Log.d(DEBUG_TAG,"Socket OutputStream not available");
                }
            }
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    private void BlockingSocketWrite(OutputStream os, byte[] bytedata){
        // Never call from UI Thread!!!
        while ( LockedOutStreams.contains(os) ){
            try {
                Thread.sleep(1000);
            }catch(InterruptedException e){
                e.printStackTrace();
            }
        }
        LockedOutStreams.add(os);
        try {
            byte[] buflen = ByteCast.int2ByteArray(bytedata.length);
            byte[] buf = new byte[SENDPACKAGE_HEADER.length + buflen.length + bytedata.length + SENDPACKAGE_FOODER.length];
            int destPos = 0;
            System.arraycopy(SENDPACKAGE_HEADER, 0, buf, destPos, SENDPACKAGE_HEADER.length);
            destPos += SENDPACKAGE_HEADER.length;
            System.arraycopy(buflen, 0, buf, destPos, buflen.length);
            destPos += buflen.length;
            System.arraycopy(bytedata, 0, buf, destPos, bytedata.length);
            destPos += bytedata.length;
            System.arraycopy(SENDPACKAGE_FOODER, 0, buf, destPos, SENDPACKAGE_FOODER.length);

            BufferedOutputStream SocketOutStream = new BufferedOutputStream(os, BUFFER_SIZE);
            int pos = 0;
            Log.d(DEBUG_TAG, "buffer len: " + String.valueOf(buf.length));
            while (pos < buf.length - BUFFER_SIZE) {
                SocketOutStream.write(buf, pos, BUFFER_SIZE);
                pos += BUFFER_SIZE;
            }
            SocketOutStream.write(buf, pos, buf.length - pos); // Write remaining bytes
            SocketOutStream.flush();
            Log.d(DEBUG_TAG, "outputstream flushed");
        } catch (IOException e) {
            e.printStackTrace();
        }
        LockedOutStreams.remove(os);
    }

    public class LocalBinder extends Binder {
        DataTransferService getService() {
            // Return this instance of LocalService so clients can call public methods
            return DataTransferService.this;
        }
    }

    private class ClientHandler implements Runnable{
        private Socket Client;
        private InetAddress ConnectedAddress;
        private boolean isThisThreadActive = true;

        public ClientHandler(Socket ClientSocket,InetAddress ConnectedAddress){
            this.Client = ClientSocket;
            this.ConnectedAddress = ConnectedAddress;
        }

        @Override
        public void run(){
            try {
                while ( !this.Client.isConnected() ){
                    try {
                        this.Client.connect(new InetSocketAddress(this.ConnectedAddress, SOCKET_PORT), SOCKET_TIMEOUT);
                    }catch(SocketTimeoutException e){
                        e.printStackTrace();
                        try{
                            Thread.sleep(1000);
                        }catch(InterruptedException ie){
                            ie.printStackTrace();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                        break;
                    }
                }
                Log.d(DEBUG_TAG,"client: "+this.Client.getInetAddress()+" is connected: "+this.Client.isConnected());
                BufferedInputStream in = new BufferedInputStream(Client.getInputStream());
                ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                byte[] buf = new byte[BUFFER_SIZE];
                int len;
                while (isThreadsActive && isThisThreadActive) {
                    if ( in.available() > 0 ) {
                        Log.d(DEBUG_TAG,"ClientThread inputstream available");
                        while ((len = in.read(buf)) > -1) {
                            //Log.d(DEBUG_TAG, "ClientThread inputstream read");
                            buffer.write(buf, 0, len);

                            byte[] tmpdata = buffer.toByteArray();
                            Log.d(DEBUG_TAG,"ClientThread inputstream tmplen: "+tmpdata.length);
                            boolean read_end = true;
                            for (int i = 0; read_end && i < SENDPACKAGE_FOODER.length; i++) {
                                read_end = ( tmpdata[ tmpdata.length - SENDPACKAGE_FOODER.length +i ] == SENDPACKAGE_FOODER[i] );
                            }
                            if ( read_end ){
                                break;
                            }
                        }
                        byte[] bufferdata = buffer.toByteArray();
                        Log.d(DEBUG_TAG,"ClientThread inputstream bytelen: "+bufferdata.length);
                        boolean data_valid = true;
                        // Splitting received bufferdata into separate messages
                        while( data_valid && ( bufferdata.length > 0) ) {
                            for (int i = 0; data_valid && i < SENDPACKAGE_HEADER.length; i++) {
                                data_valid = (i<bufferdata.length) && (bufferdata[i] == SENDPACKAGE_HEADER[i]);
                            }
                            if (data_valid) {
                                int offset = SENDPACKAGE_HEADER.length;
                                int msglen = ByteCast.ByteArray2int(Arrays.copyOfRange(bufferdata, offset, offset + ByteCast.intByteArraySize));
                                offset += ByteCast.intByteArraySize;
                                Log.d(DEBUG_TAG, "msglen: " + String.valueOf(msglen));
                                if (bufferdata[offset] == COMMANDBYTE_ENDCONNECTION) {
                                    isThisThreadActive = false;
                                    Log.d(DEBUG_TAG, "endconnection commandbyte received");
                                }else if (bufferdata[offset] == COMMANDBYTE_DEVICEADDRESS) {
                                    String addrstr = new String(Arrays.copyOfRange(bufferdata, offset+1, offset + msglen), "UTF-8");
                                    Log.d(DEBUG_TAG, "deviceaddress: "+addrstr);
                                    for( Map.Entry<Socket,String> e: Clients.entrySet() ){ // remove old entries
                                        if (e.getValue().equals(addrstr)){
                                            Clients.remove(e.getKey());
                                        }
                                    }
                                    Clients.put(Client, addrstr);
                                    UpdateClientNumber();
                                }
                                byte[] msgbuf = Arrays.copyOfRange(bufferdata, offset, offset + msglen);
                                offset += msglen;
                                OnReceivedDataViaSocket(msgbuf, Client); //process each message separately
                                bufferdata = Arrays.copyOfRange(bufferdata, offset + SENDPACKAGE_FOODER.length, bufferdata.length);
                            }
                        }
                        buffer.reset();
                    }
                }
                buffer.close();
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                if ( this.Client != null && !this.Client.isClosed() ) {
                    try {
                        this.Client.close();
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
                if ( !Clients.isEmpty() && Clients.containsKey(this.Client)){
                    Clients.remove(this.Client);
                }
                this.Client = null;
                Log.d(DEBUG_TAG, "End of Clientthread");
            }
        }
    }

}
