package com.example.magnus.movienight;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.design.widget.TabLayout;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.example.magnus.movienight.Movies.MovieList;
import com.example.magnus.movienight.Movies.MovieList_File;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity{

    private static String DEBUG_TAG = "MainActivity";

    private SharedPreferences sharedPref;

    private ArrayList<Object> mFragments = new ArrayList<>();
    private ViewPager viewPager;

    private MovieList MyMovieList;

    private MyServiceBroadcastReceiver sbcreceiver = null;
    private boolean SBCReceiverRegistered = false;
    private final IntentFilter intentFilter_myservice = new IntentFilter();

    private boolean isHostOnStart;
    private static final String isHostSaveString = "isHostSaveString";

    private DataTransferService dataService;
    private ServiceConnection dataServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            DataTransferService.LocalBinder binder = (DataTransferService.LocalBinder) service;
            dataService = binder.getService();
            Log.d(DEBUG_TAG,"connected background service");
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            dataService = null;
            Log.d(DEBUG_TAG,"disconnected background service");
        }
    };

    private WifiP2pCommunicationService wifiP2pService;
    private ServiceConnection wifiP2pServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            WifiP2pCommunicationService.LocalBinder binder = (WifiP2pCommunicationService.LocalBinder) service;
            wifiP2pService = binder.getService();
            wifiP2pService.setIsHost(isHostOnStart);
            Log.d(DEBUG_TAG,"connected wifip2p service");
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            wifiP2pService = null;
            Log.d(DEBUG_TAG,"disconnected wifip2p service");
        }
    };

    private void hideSoftKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void setupUI(View view) {
        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(v);
                    return false;
                }
            });
        }
        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        for (int i = 0; i < mFragments.size(); i++) {
            getSupportFragmentManager().putFragment(savedInstanceState, String.valueOf(i), (Fragment) mFragments.get(i));
        }
        savedInstanceState.putBoolean(isHostSaveString, wifiP2pService.getIsHost());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        MyMovieList = new MovieList_File(this);
        //MyMovieList = new MovieList_Website(this, "http://www.imdb.com/chart/top?sort=rk");

        if (savedInstanceState == null) {
            isHostOnStart = false;
            mFragments.add(new MainFragment());
            mFragments.add(new P2PFragment());
            mFragments.add(new InfoFragment());
            mFragments.add(new SettingsFragment());
        } else {
            isHostOnStart = savedInstanceState.getBoolean(isHostSaveString,false);
            for (int i = 0; i < 4; i++) {
                mFragments.add(getSupportFragmentManager().getFragment(savedInstanceState, String.valueOf(i)));
            }
        }

		startService(new Intent(this, DataTransferService.class));
        Log.d(DEBUG_TAG, "starting socket service");
        bindService(new Intent(this, DataTransferService.class), dataServiceConnection, Context.BIND_AUTO_CREATE);
        Log.d(DEBUG_TAG, "binding socket service");

        startService(new Intent(this, WifiP2pCommunicationService.class));
        Log.d(DEBUG_TAG, "starting wifip2p service");
        bindService(new Intent(this, WifiP2pCommunicationService.class), wifiP2pServiceConnection, Context.BIND_AUTO_CREATE);
        Log.d(DEBUG_TAG, "binding wifip2p service");

        intentFilter_myservice.addAction(DataTransferService.ACTION_RECEIVED_DATA);
        intentFilter_myservice.addAction(DataTransferService.ACTION_NUMBER_OF_CLIENTS);
        intentFilter_myservice.addAction(WifiP2pCommunicationService.ACTION_SUCCESSFUL_CONNECTION);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.TabLayout);
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.TabLocal)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.TabP2P)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.TabInfo)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.TabSettings)));

        viewPager = (ViewPager) findViewById(R.id.TabViewPager);
        viewPager.setOffscreenPageLimit(3);
        viewPager.setAdapter(new TabViewPagerAdapter(getSupportFragmentManager(), mFragments));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }


            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        setupUI(findViewById(R.id.TabLayout));
        setupUI(findViewById(R.id.TabViewPager));
    }

    @Override
    public void onDestroy(){
        Log.d(DEBUG_TAG, "ondestroy");
		
		unregisterReceiver(sbcreceiver);
        SBCReceiverRegistered = false;
		
        if ( dataService != null ) {
            unbindService(dataServiceConnection);
        }
        if ( wifiP2pService != null ){
            unbindService(wifiP2pServiceConnection);
        }
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.menuitem_preferences) {
            for (int i = 0; i < mFragments.size(); i++) {
                if (mFragments.get(i) instanceof SettingsFragment) {
                    viewPager.setCurrentItem(i);
                    break;
                }
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onresume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onpause");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(DEBUG_TAG, "onstart");
		if(!SBCReceiverRegistered){
            sbcreceiver = new MyServiceBroadcastReceiver();
            registerReceiver(sbcreceiver,intentFilter_myservice);
            SBCReceiverRegistered = true;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(DEBUG_TAG, "onstop");
    }

    public void OnPreferencesChanged(String key){
        Log.d(DEBUG_TAG,"OnPreferencesChanged");
        for (int i = 0; i < mFragments.size(); i++) {
            if (mFragments.get(i) instanceof P2PFragment) {
                ((P2PFragment) mFragments.get(i)).OnPreferencesChanged(key);
            }
        }
    }

    public SharedPreferences getPreferences(){
        return sharedPref;
    }

    public MovieList getMovieList(){
        return MyMovieList;
    }

    public boolean getIsWifiP2pServiceAvailable(){
        return ( wifiP2pService != null );
    }

    public boolean getIsHost(){
        return wifiP2pService != null && wifiP2pService.getIsHost();
    }

    public boolean getIsWifiP2pSupported(){
        return wifiP2pService != null && wifiP2pService.getIsWifiP2pSupported();
    }

    public boolean getIsWifiEnabled() {
        return wifiP2pService != null && wifiP2pService.getIsWifiEnabled();
    }

    public boolean getIsWifiP2pConnected() {
        return wifiP2pService != null && wifiP2pService.getIsWifiP2pConnected();
    }

    public WifiP2pDevice getWifiP2pDeviceInfo() {
        if ( wifiP2pService != null ) {
            return wifiP2pService.getWifiP2pDeviceInfo();
        }else{
            return new WifiP2pDevice();
        }
    }

    public WifiP2pInfo getConnectionInfo() {
        if ( wifiP2pService != null ) {
            return wifiP2pService.getConnectionInfo();
        }else{
            return new WifiP2pInfo();
        }
    }

    public List<WifiP2pDevice> getPeerList() {
        if ( wifiP2pService != null ) {
            return wifiP2pService.getPeerList();
        }else{
            return new ArrayList<>();
        }
    }

    public List<WifiP2pDevice> getHostingPeerList() {
        if ( wifiP2pService != null ) {
            return wifiP2pService.getHostingPeerList();
        }else{
            return new ArrayList<>();
        }
    }

    public void StartPeerDiscovery() {
        if ( wifiP2pService != null ) {
            wifiP2pService.StartPeerDiscovery();
        }
    }

    public void ConnectToDevice(WifiP2pDevice d){
        if ( wifiP2pService != null ) {
            wifiP2pService.ConnectToDevice(d);
        }
    }

    public void Disconnect(){
        if ( getIsWifiP2pConnected() ) {
            wifiP2pService.DisconnectWifiP2p();
        }
    }

    public void ChangeHosting(){
        if (dataService != null) {
            dataService.StopSocketThreads();
        }
        if ( wifiP2pService != null ) {
            wifiP2pService.setIsHost( ! wifiP2pService.getIsHost() );
            if ( getIsWifiP2pConnected() ) {
                wifiP2pService.DisconnectWifiP2p();
            }
        }
    }

    public void OnDataReceived(byte[] bytedata, String DeviceAddress) {
        Log.d(DEBUG_TAG, "data received | len: " + String.valueOf(bytedata.length));
        for (int i = 0; i < mFragments.size(); i++) {
            if (mFragments.get(i) instanceof P2PFragment) {
                ((P2PFragment) mFragments.get(i)).OnDataReceived(bytedata, DeviceAddress);
            }
        }
    }

    public void SendDataViaP2p(byte[] bytedata, String DeviceAddress){
        Log.d(DEBUG_TAG,"SendDataViaP2p");
        if ( dataService != null ){
            dataService.SendViaSocket(bytedata, DeviceAddress);
        }
    }

    // ---------------------------------------------------------------------------------------------
    // private classes
    // ---------------------------------------------------------------------------------------------

    private class TabViewPagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;
        private ArrayList<Object> mFragments;

        public TabViewPagerAdapter(FragmentManager fm, ArrayList<Object> FragmentList) {
            super(fm);
            this.mFragments = FragmentList;
            mNumOfTabs = this.mFragments.size();
        }

        @Override
        public Fragment getItem(int position) {
            return (Fragment) mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }

    public class MyServiceBroadcastReceiver extends BroadcastReceiver{

        private String DEBUG_TAG = "MyServiceBroadcastReceiver";

        @Override
        public void onReceive(Context context, Intent intent) {

            switch ( intent.getAction() ) {
                case DataTransferService.ACTION_RECEIVED_DATA: {
                    byte[] data = intent.getByteArrayExtra(DataTransferService.EXTRAS_BYTEDATA);
                    String DeviceAddress = intent.getStringExtra(DataTransferService.EXTRAS_CLIENT_ADDRESS);
                    MainActivity.this.OnDataReceived(data,DeviceAddress);
                    break;
                }
                case DataTransferService.ACTION_NUMBER_OF_CLIENTS: {
                    int NumberOfClients = intent.getIntExtra(DataTransferService.EXTRAS_NUMBER_OF_CLIENTS, 0);
                    Log.d(DEBUG_TAG, "updated clients: " + String.valueOf(NumberOfClients));
                    for (int i = 0; i < mFragments.size(); i++) {
                        if (mFragments.get(i) instanceof P2PFragment) {
                            ((P2PFragment) mFragments.get(i)).OnClientsChanged(NumberOfClients);
                        }
                    }
                    break;
                }
                case WifiP2pCommunicationService.ACTION_SUCCESSFUL_CONNECTION: {
                    if ( wifiP2pService.getIsHost() ){
                        dataService.SetToServerMode();
                    }else {
                        dataService.SetToClientMode(getConnectionInfo().groupOwnerAddress, getWifiP2pDeviceInfo().deviceAddress);
                    }
                    break;
                }
                case WifiP2pCommunicationService.ACTION_PEERS_CHANGED: {
                    for (int i = 0; i < mFragments.size(); i++) {
                        if (mFragments.get(i) instanceof P2PFragment) {
                            ((P2PFragment) mFragments.get(i)).OnPeersChanged();
                        }
                    }
                    break;
                }
                default:
                    Log.d(DEBUG_TAG,"onReceive - invalid Action");
            }
        }

    }

}